# Create React Travel Website

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

### `yarn test`

### `yarn build`

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).



### Deployment

 [![Netlify Status](https://api.netlify.com/api/v1/badges/06643604-2c1d-44fe-93a0-50b6435d1498/deploy-status)](https://travelo-ca.netlify.app/)
