import React from "react";
import Logo from "../assets/logo.png";
import { Nav } from "./styles/NavBar.styled";

const Navbar = () => {
  return (
    <>
      <Nav>
        <div className="brand">
          <div className="container">
            <img src={Logo} alt="logo" />
            Travelo
          </div>
          <div className="toggle"></div>
        </div>
        <ul>
          <li><a href="#home">Home</a></li>
          <li><a href="#services">Services</a></li>
          <li><a href="#recommend">Recommend</a></li>
          <li><a href="#testimonials">Testimonials</a></li>
        </ul>
        <button>Connect</button>
      </Nav>
    </>
  );
};

export default Navbar;
